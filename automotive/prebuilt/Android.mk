# Add precompiled modules to this file as examples below;
#
#LOCAL_PATH := $(call my-dir)
#
#include $(CLEAR_VARS)
#LOCAL_MODULE := mod_1_name
#LOCAL_VENDOR_MODULE := true
#LOCAL_MODULE_CLASS := APPS
#LOCAL_MODULE_TAGS := optional
#LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
#LOCAL_CERTIFICATE := platform
##LOCAL_CERTIFICATE := PRESIGNED
#LOCAL_SRC_FILES := mod_1.apk
#include $(BUILD_PREBUILT)
#
#include $(CLEAR_VARS)
#LOCAL_MODULE := mod_2_name
#LOCAL_VENDOR_MODULE := true
#LOCAL_MODULE_CLASS := EXECUTABLES
#LOCAL_SRC_FILES := mod_2_exec
#include $(BUILD_PREBUILT)
