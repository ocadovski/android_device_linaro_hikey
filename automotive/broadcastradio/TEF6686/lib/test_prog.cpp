#include "tef6686.h"

pthread_t thread_rds;
pthread_mutex_t radio_lock;
int clear_rds = 0;
int fm = 1;

void *rdsloop(void*){
	int r, i;

	uint16_t rds_stat, rds_a, rds_b, rds_c, rds_d, rds_err;
	char pstext[9]; memset(pstext,0,9);
	char rtext[65]; memset(rtext,0,65);
	int rds_offset;
	int rds_type = 0;
	char block_c[2];
	char block_d[2];
	char toggleWipe = 0;

	while (fm){
		pthread_mutex_lock(&radio_lock);
		r = devTEF668x_Radio_Get_RDS_Data(&rds_stat, &rds_a, &rds_b, &rds_c, &rds_d, &rds_err);
		pthread_mutex_unlock(&radio_lock);

		if (clear_rds){
			memset(pstext, 0, 9);
			memset(rtext, 0, 65);
			clear_rds = 0;
		}

		if (r && rds_b != 0x0 && (rds_stat & 0x8000) != 0x0){
			if ((rds_stat & 0x4000) != 0) printf("RDS packet loss.\n");

//			printf("Stat 0x%04X: A 0x%04X / B 0x%04X / C 0x%04X / D 0x%04X, ERR: 0x%04X\n", rds_stat, rds_a, rds_b, rds_c, rds_d, rds_err);

			/* Block A: PI code -- station unique identifier. Ignore for now.
			 * Block B: b15..b12 -- group type "0000" = station name, "0010" = radio text,
			 * 		 b11 -- 0 = type A, 1 = type B
			 * 		 b10 -- 1 = traffic program
			 * 	    b09..b05 -- program type code (i.e. news, weather, rock, etc.)
			 * 	    b04..b00 -- 
			 *
			 */

			if (((rds_b >> 8) & 0xf0) == 0x0){ // this is a PS message
				// If there is NOT an uncorrected error in block B or D (we don't care about A or C for PS)
				if ((rds_err & 0x0a00) == 0x0){ // other option is (rds_err & 0f00)
					rds_offset = (rds_b & 0x3) * 2;
					// Only process the update if data actually changes.
					if (pstext[rds_offset] != (rds_d >> 8) || pstext[rds_offset+1] != (rds_d & 0xff)){
						pstext[rds_offset] = rds_d >> 8;
						pstext[rds_offset+1] = rds_d & 0xff;
						printf("PS TEXT: %s\n", pstext);
					}
				}
			} else if (((rds_b >> 8) & 0xf0 )== 0x20){ // This is RADIO TEXT
				rds_type = (rds_b & 0x0800) >> 8;
				rds_offset = (rds_b & 0xf);
				if (rds_type == 0){
					rds_offset *= 4; // radio text type A has 4 bytes per update in blocks C and D
					//printf("RDS OFFSET: %d\n", rds_offset);
					// Radio text type A requires valid B, C, and D blocks.
					if ((rds_err & 0x0a00) == 0x0){ // other option is (rds_err & 0f00)
						if ((rds_b & 0x10) != toggleWipe) {
							memset(rtext, 0, 65);
							toggleWipe = (rds_b & 0x10);
						}
						for (i = 0; i < rds_offset; i++) if (rtext[i] == 0) rtext[i] = 0x20;
						block_c[0] = rds_c >> 8;
						block_c[1] = rds_c & 0xff;
						block_d[0] = rds_d >> 8;
						block_d[1] = rds_d & 0xff;
						if (block_c[0] == 0x0d) block_c[0] = 0;
						if (block_c[1] == 0x0d) block_c[1] = 0;
						if (block_d[0] == 0x0d) block_d[0] = 0;
						if (block_d[1] == 0x0d) block_d[1] = 0;
						if (rtext[rds_offset] != block_c[0] || rtext[rds_offset+1] != block_c[1]
								|| rtext[rds_offset+2] != block_d[0] || rtext[rds_offset+3] != block_d[1]){

							rtext[rds_offset] = block_c[0];
							rtext[rds_offset+1] = block_c[1];
							rtext[rds_offset+2] = block_d[0];
							rtext[rds_offset+3] = block_d[1];

							printf("Radio Text (A): %s\n", rtext);
						}
					}
				} else {
					rds_offset *= 2; // radio text type B has 2 bytes per update in block D
					// Radio text type B requires valid B and D blocks.
					if (!((rds_err & 0x3000) == 0x3000 || (rds_err & 0x0300) == 0x0300)){
						if ((rds_b & 0x10) != toggleWipe) {
							memset(rtext, 0, 65);
							toggleWipe = (rds_b & 0x10);
						}
						for (i = 0; i < rds_offset; i++) if (rtext[i] == 0) rtext[i] = 0x20;
						block_d[0] = rds_d >> 8;
						block_d[1] = rds_d & 0xff;
						if (block_d[0] == 0x0d) block_d[0] = 0;
						if (block_d[1] == 0x0d) block_d[1] = 0;
						if (rtext[rds_offset] != block_d[0] || rtext[rds_offset+1] != block_d[1]){
							rtext[rds_offset] = block_d[0];
							rtext[rds_offset+1] = block_d[1];

							printf("Radio Text (B): %s\n", rtext);
						}
					}
				}
			} else {
				//printf("Unhandled GTYPE. RDS Block B: 0x%04X\n", rds_b);
			}
		}
		if (rds_b == 0x0) usleep(100000);
	}
	return 0;
}

void startRdsThread(){
	if (pthread_create(&thread_rds, NULL, rdsloop, NULL) != 0) printf("Error creating RDS thread. No RDS\n");
	pthread_detach(thread_rds);
}
/*-----------------------------------------------------------------------
Function name:	main
Input:			N/A
Output:			N/A
Description:	
------------------------------------------------------------------------*/
int main(int argc, char** argv){
	char *line = NULL;
	size_t len = 0;
	ssize_t read = 0;
	int r = 0, i;
	uint16_t freq;

	int parampath = 0;
	int reinit = 0;
	int noinit = 0;
	int testing = 0;

	char i2c_p[64];

	uint8_t stat8, usn, wam;
	int16_t level, offset;
	uint16_t iatt, fatt, stat16;

	for (i = 1; i < argc; i++){
		if (strcmp(argv[i], "--reinit") == 0) reinit = 1;
		else if (strcmp(argv[i], "--noinit") == 0) noinit = 1;
		else if (strcmp(argv[i], "--testing") == 0){
			testing = i;
			i+=2;
		} else {
			snprintf(i2c_p, 64, "%s", argv[1]);
			parampath = 1;
		}
	}

	if (parampath == 0) snprintf(i2c_p, 64, "/dev/i2c-2");

	setup_i2c(i2c_p);

	/*
	int devTEF668x_Radio_Get_Quality_Status (int fm, uint8_t *status);
	int devTEF668x_Radio_Get_Quality_Level (int fm, uint8_t *status, int16_t *level);
	int devTEF668x_Radio_Get_Quality_Data (int fm, uint8_t *usn, uint8_t *wam, int16_t *offset);
	int devTEF668x_Radio_Get_AGC(int fm, uint16_t *input_att, uint16_t *feedback_att);
	int devTEF668x_Radio_Get_Signal_Status(int fm, uint16_t *status);
	int devTEF668x_APPL_Get_GPIO_Status(uint16_t *status);
	*/

	if (devTEF668x_Radio_Get_Quality_Status (1, &stat8))
		printf("Get_Quality_Status: %02X\n", stat8);

	if (devTEF668x_Radio_Get_Quality_Level (1, &stat8, &level))
		printf("Get_Quality_Level status: %02X, level: %04X\n", stat8, level);

	if (devTEF668x_Radio_Get_Quality_Data (1, &usn, &wam, &offset))
		printf("Get_Quality_Data usn: %02X, wam: %02X, offset: %04X\n", usn, wam, offset);

	if (devTEF668x_Radio_Get_AGC(1, &iatt, &fatt))
		printf("Get_AGC input_att: %04X, feedback_att: %04X\n", iatt, fatt);

	if (devTEF668x_Radio_Get_Signal_Status(1, &stat16))
		printf("Get_Signal_Status: %04X\n", stat16);

	if (devTEF668x_APPL_Get_GPIO_Status(&stat16))
		printf("APPL_Get_GPIO_Status: %04X\n", stat16);

	if (!testing) Radio_Para_Init(); // This really doesn't do anything useful.
	// Seems to have originally been intended to load the last
	// state from eeprom. Now all it does is sets up the "current"
	// station as the second hard-coded preset.

	while (r == 0){
		r = Tuner_Power_on(); // power on the tuner.
		// r == 0: busy
		// r == 1: success
		// r == 2: does not exist
		if (r == 2) exit(1);
	}

	if (testing){
		Tuner_Init(atoi(argv[testing+1]), atoi(argv[testing+2]));
		exit(0);
	}

	if (!Tuner_Init()){ // load tuner initialization.
		exit(1);
	}

	Radio_SetFreq(Radio_PRESETMODE, MW_BAND /*AM "Medium Wave"*/, 1010); // Tune to 1010 AM

	if (reinit != 1) { // if this is not just a reinit, go into loop
		sleep(3);

		Radio_SetFreq(Radio_PRESETMODE, Radio_GetCurrentBand(), Radio_GetCurrentFreq());

		pthread_mutex_init(&radio_lock, (const pthread_mutexattr_t *) NULL);

		startRdsThread();
	
		// Main input loop
		printf("Command input. Available commands;\nu: tune up\nd: tune down\ni: seek up\nf: seek down\nt: tune direct (use t1010A or t9990F)\n > ");
		while((read = getline(&line, &len, stdin)) != -1){
			printf("Read line: %s\n", line);
			clear_rds = 1;
			pthread_mutex_lock(&radio_lock);
			switch(line[0]){
				case 'u':
					printf("Calling for tune UP\n");
					tune(1);
					break;
				case 'd':
					printf("Calling for tune DOWN\n");
					tune(0);
					break;
				case 'i':
					printf("Calling for seek UP\n");
					seek(1);
					break;
				case 'f':
					printf("Calling for seek DOWN\n");
					seek(0);
					break;
				case 't':
					printf("Calling for direct tune\n");
					freq = atoi(&line[1]);
					if (!fm && strchr(line, 'A') == 0){ // just switched from am to fm
						fm = 1;
						startRdsThread();
					} else if (strchr(line, 'A') != 0){ // this is an am station
						fm = 0; // this will cause the RDS thread to terminate
					}
					tune_direct(freq, fm);
					break;

			}
			pthread_mutex_unlock(&radio_lock);
	
			printf("\n > ");
		}
	}
}

