#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <android/log.h>

#include <sys/ioctl.h>
#include <sys/types.h>

#include <linux/types.h>
#include <linux/input.h>
#include <linux/hidraw.h>
#include <dirent.h>

#include <sys/system_properties.h>
#include <cutils/properties.h>

int hid_fd = -1;

void writehid(uint8_t cmd, uint8_t value, uint8_t len){
    uint8_t buf[16];
    int res;

    buf[0] = 0x0;   // Report ID (0)
    buf[1] = cmd;   // Command (0x20 - query, 0x02 - set FAN)
    buf[2] = value; // Value to set

    res = write(hid_fd, buf, len+1);
    if (res < 0){
        __android_log_print(ANDROID_LOG_DEBUG, "thermalfand", "HID write error: %d", errno);
        if (errno != 75 && errno != 71 && errno != 32){
            close(hid_fd);
            hid_fd = -1;
        }
    }
}

int hidrawdev(const struct dirent *d){
    if (strstr(d->d_name, "hidraw") != NULL) return 1;
    return 0;
}

void openhid(){
    int n, res;
    struct dirent **filelist;
    char buf[256];
    char path[16];
    struct hidraw_report_descriptor rpt_desc;
    struct hidraw_devinfo info;

    n = scandir("/dev", &filelist, *hidrawdev, NULL);
    __android_log_print(ANDROID_LOG_DEBUG, "thermalfand", "Found %d hidraw devices.", n);
    while (n--){
        snprintf(path, sizeof(path), "/dev/%s", filelist[n]->d_name);
        free(filelist[n]);
        __android_log_print(ANDROID_LOG_DEBUG, "thermalfand", "File: %s", path);

        if (hid_fd < 0){
            hid_fd = open(path, O_RDWR|O_NONBLOCK);
            if (hid_fd < 0) continue;
            __android_log_print(ANDROID_LOG_DEBUG, "thermalfand", "file opened");

            memset(&rpt_desc, 0x0, sizeof(rpt_desc));
            memset(&info, 0x0, sizeof(info));
            memset(buf, 0x0, sizeof(buf));

            /* Get Raw Info */
            res = ioctl(hid_fd, HIDIOCGRAWINFO, &info);

            if ((int16_t)info.vendor != (int16_t)0x239a){
                __android_log_print(ANDROID_LOG_DEBUG, "thermalfand", "wrong vendor");
                close(hid_fd);
                hid_fd = -1;
                continue;
            }

            if ((int16_t)info.product != (int16_t)0x801e){
                __android_log_print(ANDROID_LOG_DEBUG, "thermalfand", "wrong product");
                close(hid_fd);
                hid_fd = -1;
                continue;
            }
            __android_log_print(ANDROID_LOG_DEBUG, "thermalfand", "device OK, checking endpoint");

            // Command 0x20 requests which type, 0x02 = fan
            writehid(0x20, 0x00, 1);

            // Try to read data from this endpoint
            for (int i=0; i<10; i++){
                if ((res = read(hid_fd, buf, 16)) > 0){
                    __android_log_print(ANDROID_LOG_DEBUG, "thermalfand", "received data: %02X\n", buf[0]);
                    // If we read 0x02, leave the fd open, further iterations of the outer
                    // loop are just going to free the remaining dirent's.
                    if (buf[0] != 0x02){
                        close(hid_fd);
                        hid_fd = -1;
                    }
                    break;
                }
                usleep(10000);
            }
            if (res <= 0 && hid_fd >= 0){
                close(hid_fd);
                hid_fd = -1;
            }
        }
    }
    free(filelist);
}

int main(){

        int lasttemp = 0, curtemp;
        unsigned char lastpwm = 0x00;
        unsigned char curpwm = 0x00;
        char buff[10];

        int tempfd = open("/sys/devices/virtual/thermal/thermal_zone0/temp", O_RDONLY);
        if (tempfd <= 0) return 0;
        __android_log_print(ANDROID_LOG_DEBUG, "thermalfand", "Entered thermal monitoring loop");

        while (1){
                lseek(tempfd, 0, SEEK_SET);
                read(tempfd, buff, 6);
                curtemp = atoi(buff);
                __android_log_print(ANDROID_LOG_DEBUG, "thermalfand", "Read temperature: %d", curtemp);
                if (curtemp > lasttemp){
                        // temperature is increasing
                        if (curtemp >= 65000) curpwm = 0xff; // 100%
                        else if (curtemp >= 60000) curpwm = 0xe5; // 90%
                        else if (curtemp >= 55000) curpwm = 0xcc; // 80%
                        else if (curtemp >= 50000) curpwm = 0xb2; // 70%
                        else if (curtemp >= 45000) curpwm = 0x99; // 60%
                        else if (curtemp >= 40000) curpwm = 0x7f; // 50%
                } else {
                        // temperature is decreasing
                        if (curtemp < 35000) curpwm = 0x00; // 0%
                        else if (curtemp < 40000) curpwm = 0x7f; // 50%
                        else if (curtemp < 45000) curpwm = 0x99; // 60%
                        else if (curtemp < 50000) curpwm = 0xb2; // 70%
                        else if (curtemp < 55000) curpwm = 0xcc; // 80%
                        else if (curtemp < 60000) curpwm = 0xe5; // 90%
                }
                if (curpwm != lastpwm){
                        __android_log_print(ANDROID_LOG_DEBUG, "thermalfand", "Sending fan update: %02X", curpwm);
                        if (hid_fd < 0) openhid();
                        if (hid_fd >= 0) writehid(0x02, curpwm, 2);
                        lastpwm = curpwm;
                        lasttemp = curtemp;
                }
                sleep(1);
        }
}

